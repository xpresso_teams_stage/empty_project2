""" Class design for Dataset"""
import copy
import datetime
import json
import os
from abc import abstractmethod
from enum import Enum

import numpy as np
import pandas as pd

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    SerializationFailedException, DeserializationFailedException
from xpresso.ai.core.commons.utils.constants import NGRAMS, CORRELATIONS
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.exploration.attribute_info import AttributeInfo
from xpresso.ai.core.data.exploration.structured_dataset_info import \
    StructuredDatasetInfo
from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ['AbstractDataset', 'DatasetEncoder', 'DatasetDecoder']
__author__ = 'Srijan Sharma'

LOGGER = XprLogger()


class DatasetEncoder(json.JSONEncoder):
    """Class to encode dataset object to json"""

    def default(self, o):
        if isinstance(o, datetime.date):
            return o.strftime("%Y-%m-%d %H:%M:%S")
        elif isinstance(o, Enum):
            return o.value
        elif isinstance(o, np.dtype):
            return o.name
        elif isinstance(o, np.ndarray):
            return o.tolist()
        return o.__dict__


class DatasetDecoder(json.JSONDecoder):
    """Class to decode json to dataset object"""

    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args,
                                  **kwargs)

    def object_hook(self, obj):
        try:
            obj['created_on'] = datetime.datetime.strptime(obj['created_on'],
                                                           "%Y-%m-%d %H:%M:%S")
            obj['type'] = DatasetType(obj['type'])
        except KeyError:
            return obj
        return obj


class AbstractDataset(object):
    """ Dataset is an abstract storage class. It is responsible for complete
    lifecycle of the a automl. It start with importing from the source,
    saving/loading from the local, performing exploration and analysis
    over the data
    """

    def __init__(self, dataset_name: str = "default",
                 description: str = "This is a automl",
                 project_name: str = "default_project",
                 created_by: str = "default",
                 config_path: str = XprConfigParser.DEFAULT_CONFIG_PATH):
        self.config = XprConfigParser(config_file_path=config_path)

        self.data = pd.DataFrame()
        self.name = dataset_name
        self.type = DatasetType.STRUCTURED
        self.description = description
        self.num_records = len(self.data)
        self.created_on = datetime.datetime.now().date()
        self.created_by = created_by
        self.project = project_name
        self.repo = "default"
        self.branch = "master"
        self.version = 1
        self.tag = "1.0.0"
        self.info = StructuredDatasetInfo()
        self.local_storage_required = False
        self.sample_percentage = 100.00
        self.CONTROLLER_SECTION = "controller"
        self.CLIENT_PATH = "client_path"
        self.path = os.path.join(
            os.path.expanduser('~'),
            self.config[self.CONTROLLER_SECTION][self.CLIENT_PATH])
        self.token_file = '{}.current'.format(self.path)

    @abstractmethod
    def import_dataset(self, data_source, local_storage_required: bool = False,
                       sample_percentage: float = 100):
        """
        Fetches automl from multiple data sources and loads them
        into a automl

        Args:
            data_source(str): string path or uri of the data source
            local_storage_required(bool):
            sample_percentage(sbool):
        """

    @abstractmethod
    def save(self):
        """ Serialize the automl and store it into a local storage"""

    @abstractmethod
    def load(self, directory_path):
        """
        Load the data set from local storage and deserialize to update
        the dataset
        Args:
            directory_path(str): path where json file (i.e. metrics,
            metadata) and data
        """

    @abstractmethod
    def diff(self, new, output_path=None):
        """ Find the diff between two automl"""

    def get_local_storage_path(self):
        """ Returns the path of the local storage"""
        # xpresso platform standard path where token file is saved
        local_storage = os.path.join(self.project, "datasets", self.name)
        os.makedirs(local_storage, exist_ok=True)
        return local_storage

    def get_csv_file_path(self, number=1):
        """
        Get the csv file path
        Args:
            number: Specify which version the csv file. default=1

        Returns:
            str: absolute csv file path
        """
        return self.get_file_pattern() % '{0:0>5}.csv'.format(number)

    def get_json_file_path(self, number=1):
        """
        Get the json file path
        Args:
            number: Specify which version the csv file. default=1

        Returns:
            str: absolute json file path
        """
        return self.get_file_pattern() % '{0:0>5}.json'.format(number)

    def get_file_pattern(self):
        """ Generates a name pattern for all the file. This is
         used to generate the absolute file path maintaining the versions"""
        parent_dir = self.get_local_storage_path()
        return os.path.join(parent_dir, f"{self.name}_dataset__%s")

    def import_from_dataset(self, dataset):
        """
        Import properties of dataset from another dataset

        Args:
            dataset: source dataset. Properties of these dataset will be
                     updated in the current dataset
        """
        self.__dict__ = copy.deepcopy(dataset.__dict__)

    def json_serialize_structured(self):
        """ Helper function to serialize structured dataset metadata and
        metrics"""
        temp_dataset = copy.deepcopy(self)
        del temp_dataset.data
        for attr in temp_dataset.info.attributeInfo:
            try:
                del attr.logger
            except AttributeError:
                LOGGER.warning("AttributeInfo not present")

        for attr in temp_dataset.info.attributeInfo:
            if attr.type == "text" and attr.metrics:
                attr.metrics = self.metric_serialize_deserialize(attr.metrics,
                                                                 serialize=True,
                                                                 keys=NGRAMS)

        if temp_dataset.info.metrics:
            temp_dataset.info.metrics = self.metric_serialize_deserialize(
                temp_dataset.info.metrics, serialize=True, keys=CORRELATIONS)

        try:
            json_file_path = self.get_json_file_path()
            with open(json_file_path, 'w', encoding='utf-8') as file:
                json.dump(temp_dataset, file, ensure_ascii=False, indent=4,
                          cls=DatasetEncoder)
        except (TypeError, OverflowError, ValueError):
            raise SerializationFailedException("JSON serialization failed")
        return json_file_path

    def json_deserialize_structured(self, json_data_path):
        """
        Helper function to deserialize json object to
        structured dataset object
        Args:
            json_data_path(str): json file path to load"""
        try:
            with open(json_data_path) as json_file:
                json_data = json.load(json_file, cls=DatasetDecoder)
        except json.JSONDecodeError:
            raise DeserializationFailedException("JSON deserialization failed")

        # before understand
        for key, value in json_data.items():
            setattr(self, key, value)

        try:
            self.config = XprConfigParser()
            self.config.config_json = json_data["config"]["config_json"]
        except KeyError:
            LOGGER.warning("'Config' key error in json data ")

        try:
            info = json_data["info"]
            self.info = StructuredDatasetInfo()
            for key, value in info.items():
                setattr(self.info, key, value)
        except KeyError:
            LOGGER.warning("Info key error in json data")
        try:
            attribute_info = json_data["info"]["attributeInfo"]
            self.info.attributeInfo = list()
            for attr in attribute_info:
                var = AttributeInfo(attr["name"])
                for key, value in attr.items():
                    setattr(var, key, value)
                self.info.attributeInfo.append(var)
        except KeyError:
            LOGGER.warning("Info key error in json data")

        for attr in self.info.attributeInfo:
            if attr.type == "text" and attr.metrics:
                attr.metrics = self.metric_serialize_deserialize(attr.metrics,
                                                                 serialize=False,
                                                                 keys=NGRAMS)
        if self.info.metrics:
            self.info.metrics = self.metric_serialize_deserialize(
                self.info.metrics,
                serialize=False,
                keys=CORRELATIONS)

    def json_serialize_unstructured(self):
        pass

    def json_deserialize_unstructured(self, json_data_path):
        pass

    @staticmethod
    def list_from_dict(metric):
        """ Returns a list of dictionaries when key is tuple, with each
        dictionary as "key"-"value" pair
        Args:
            metric(dict): dictionary with keys as tuple
        """
        ret = [{"key": q, "value": p} for q, p in
               zip(metric.keys(), metric.values())]
        return ret

    @staticmethod
    def dict_from_list(metric):
        """
        Returns a dictionary from list of dictionaries where "keys" of each
        dictionary is key of the new dictionary and "values" of each
        dictionary is value of the new dictionary
        Args:
            metric(list: dict): list of dictionaries
        """
        ret = {tuple(x["key"]): x["value"] for x in metric}
        return ret

    def metric_serialize_deserialize(self, metric, serialize, keys):
        """
        Helper function to serialize and deserialize ngram and
        correlation metrics
        Args:
            metric(obj): dictionary of metrics
            serialize(bool): True to serialize, False to deserialize
            keys(list): list of metric keys
        Returns:
            metric(obj): serialized/deserialized metric
        """
        for key in keys:
            try:
                if serialize:
                    metric[key] = self.list_from_dict(
                        metric[key])
                else:
                    metric[key] = self.dict_from_list(
                        metric[key])
            except KeyError:
                LOGGER.warning("{} key error in json data".format(key))
        return metric
