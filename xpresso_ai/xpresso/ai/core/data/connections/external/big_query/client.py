"""Class design for BigQuery Connectivity"""

__author__ = 'Shlok Chaudhari'

import os
import json
import xpresso.ai.core.commons.utils.constants as constants
from google.cloud import bigquery
from xpresso.ai.core.data.connections.external.big_query import bigquery_exception
from xpresso.ai.core.logging.xpr_log import XprLogger


class BigQueryConnector:
    """

    This class is used to interact with BigQuery. BigQuery is a serverless,
    petabyte scalable tool provided under the GCP stack which helps to store,
    manage and interact with external data sources, transactional databases or
    spreadsheets in Google Drive.

    """

    def __init__(self, user_config):
        """

        __init__() here initializes the XprLogger and client object. Sets environment
        variables as required by GCP BigQuery. Takes a user-specified
        DSN from the config/common.json to set public project_id and corresponding
        dataset.

        """

        self.logger = XprLogger()
        self.client = None
        self.user_project_id = user_config.get(constants.project_id)
        self.dataset = user_config.get(constants.dataset)
        path = user_config.get(constants.cred_path)
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = path
        with open(path) as cred:
            self.cred_project_id = json.load(cred).get(constants.project_id)

    def connect(self):
        """

        Connection using client API to GCP BigQuery.

        :return: None

        """

        try:
            self.client = bigquery.Client(project=self.cred_project_id)
        except bigquery_exception.BigQueryClientException as exc:
            self.logger.exception("\nBigQuery Client connection failed\n\n" + str(exc))

    def import_data(self, table, columns):
        """

        Imports a dataframe from various tables in the BigQuery datasets

        :param columns: names of columns as a comma-separated string
                        Put '*' in case of all columns. (String object)
        :param table: table name present in the user project_id (String object)

        :return: pandas DataFrame object

        """

        self.connect()
        dataframe = None
        try:
            query = "select %s from `%s.%s.%s` limit 100"
            query_job = self.client.query(query % (columns, self.user_project_id,
                                                   self.dataset, table))
            dataframe = query_job.result().to_dataframe()
        except bigquery_exception.BigQueryImportFail as exc:
            self.logger.exception("\nBigQuery import dataframe failed\n\n" + str(exc))
        return dataframe

    def close(self):
        """

        BigQuery does not have any close methods for client API

        :return: None

        """

        return None
